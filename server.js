const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose')

require('dotenv').config();

const app = express();
const port = process.env.APP_PORT;

const uri = `mongodb+srv://dahohawley:uosvu49Yqzi7Pi9e@cluster0.pjdqw.mongodb.net/test>?retryWrites=true&w=majority`;

mongoose.connect(uri,{useNewUrlParser: true,useCreateIndex : true,useUnifiedTopology:true});
connection = mongoose.connection;
connection.once('open',()=>{
    console.log('Mongoose connected')
});

app.use(cors());
app.use(express.json());


const userRouter = require('./routes/user');
const todoItem  = require('./routes/todo/item');

app.use('/user',userRouter);
app.use('/todo-item',todoItem);

app.listen(port, () => {
    console.log(`Sever is running on port : ${port}`);
});
  
