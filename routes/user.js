const router = require('express').Router()
const bcrypt    = require('bcrypt');
const { json } = require('express');
const User = require('../models/user');
const jwt = require('jsonwebtoken')

let user = require('../models/user');
let refreshToken = require('../models/refreshToken');
const RefreshToken = require('../models/refreshToken');

const generic = require('../generic/authorization');


router.post('/login',(req,res)=>{
    const username = req.body.username;
    const password = req.body.password;

    User.findOne({
        username : username
    }).then((data)=>{
        return new Promise((resolve,reject)=>{
            if(data){
                bcrypt.compare(password,data.password,(err,same)=>{
                    if(same){
                        const accessToken = generateAccessToken(data)
                        const refreshToken = jwt.sign(
                            {
                                username : data.username,
                                user_id : data._id
                            },
                            process.env.REFRESH_TOKEN_SECRET
                        )

                        // save refresh token to db!
                        const mRefreshToken = new RefreshToken({
                            refreshToken
                        });
                        mRefreshToken.save();
                        
                        res.json({
                            info : 'authenticated',
                            data : {
                                accessToken,
                                refreshToken,
                                userData : {
                                    username : data.username,
                                    userId  : data._id
                                },
                            }
                        })
                    }else{
                        res.status(403).json("Invalid Username / Password");
                    }
                })
            }else{
                reject("Invalid Username / Password");
            }
        });
    }).catch((response)=>{
        res.status(403).json(response);
    })
});

router.get('/isAuthenticated',generic.authorizationToken,(req,res)=>{
    const refreshToken = req.headers['refreshtoken'];
    if(refreshToken == null) return res.sendStatus(401);
    
    // Cari refresh tokennya di db
    RefreshToken.findOne({
        refreshToken
    }).then((data)=>{
        if(!data) res.sendStatus(401);
        jwt.verify(refreshToken,process.env.REFRESH_TOKEN_SECRET,(err,payload)=>{
            if(err) res.sendStatus(500)
            const accessToken = generateAccessToken(
                { 
                    username : payload.username, 
                    _id : payload.user_id 
                }
            );
            res.json({
                accessToken
            })
        });
    }).catch((err)=>{
        res.json(err)
    })  
})

router.post('/renew-token',(req,res)=>{
    const refreshToken = req.body.token;
    if(refreshToken == null) return res.sendStatus(401);
    // Cari refresh tokennya di db
    RefreshToken.findOne({
        refreshToken
    }).then((data)=>{
        if(!data) res.sendStatus(401);
        jwt.verify(refreshToken,process.env.REFRESH_TOKEN_SECRET,(err,payload)=>{
            if(err) res.sendStatus(500)
            console.log(payload)
            const accessToken = generateAccessToken(
                { 
                    username : payload.username, 
                    _id : payload.user_id 
                }
            );
            res.json({
                accessToken
            })
        });
    })
});

function generateAccessToken(data){
    return jwt.sign(
        {
            username : data.username,
            user_id : data._id
        },
        process.env.ACCESS_TOKEN_SECRET,
        {expiresIn:'15m'}
    );
}

module.exports = router