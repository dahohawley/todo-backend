const { Router } = require('express');
const router = require('express').Router();
const Item = require('../../models/Todo/item');

const generic = require('../../generic/authorization');

router.post("/create",generic.authorizationToken,(req,res)=>{
    const jwtPayload    = req.jwt_payload;
    const name = req.body.name
    const type = req.body.type
    const recurringDate = req.body.recurring

    if((typeof name === 'undefined') || name.length <= 0){
        res.status(400).json("Name required!");
    }

    if((typeof type === 'undefined') || type.length <= 0){
        res.status(400).json("type required!");
    }

    const todoitem    = new Item({
        name,
        type,
        recurringDate
    });

    todoitem.save().then((data)=>{
        res.json(data)
    });
});

router.get('/',generic.authorizationToken,(req,res)=>{
    const Items = Item.find().then((data)=>{
        res.json(data);
    })
});



module.exports = router
