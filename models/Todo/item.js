const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema(
    {
        name : {
            type : String,
            required : true
        },
        type : {
            type : String,
            required : true

        },
        recurring : Number
    },
    {
        timestamps :true
    }
)

const Item = mongoose.model('Item',ItemSchema);
module.exports = Item;
