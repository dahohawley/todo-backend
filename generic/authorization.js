const { json } = require('express');
const jwt = require('jsonwebtoken')

authorizationToken = (req,res,next) => {
    const authHeader = req.headers['authorization'];
    if(typeof authHeader == 'undefined'){
        res.status(403).json("Unauthorized!!");
        return;
    }
    const token = authHeader.split(' ')[1];
    if(!token){
        res.status(403).json("Unauthorized!");
    }
    
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,payload)=>{
        if(err){
            res.status(403).json("Unauthorized!!!")
            return;
        }
        req.jwt_payload = payload;
        next();
    })
};

module.exports.authorizationToken = authorizationToken